﻿using System;
using System.ServiceModel;
using MockInitiateServiceLib;

namespace InitiateServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost serviceHost = new ServiceHost(typeof (MockInitiateService));

            using (serviceHost)
            {
                DisplayHostInfo(serviceHost);
                Console.WriteLine();
                Console.WriteLine("Service Host Started ...");
                Console.ReadLine();
            }
          
        }

        static void DisplayHostInfo(ServiceHost serviceHost)
        {
            Console.WriteLine();
            Console.WriteLine("***** Host Info *****");

            Console.WriteLine("Name: {0}",
              serviceHost.Description.ConfigurationName);
            Console.WriteLine("Port: {0}",
              serviceHost.BaseAddresses[0].Port);
            Console.WriteLine("LocalPath: {0}",
              serviceHost.BaseAddresses[0].LocalPath);
            Console.WriteLine("Uri: {0}",
              serviceHost.BaseAddresses[0].AbsoluteUri);
            Console.WriteLine("Scheme: {0}",
              serviceHost.BaseAddresses[0].Scheme);
            Console.WriteLine("**********************");
            Console.WriteLine();
           

        }
    }
}
