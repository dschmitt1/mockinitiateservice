﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class InitiateService : IIdentityHubPort
    {
        public List<Member> MemberGetRequest()
        {
            var members = new List<Member>();
            members.Add(new Member(){Name = "Test"});
            return members;
        }

        public List<Member> MemberSearchRequest()
        {
            var members = new List<Member>();
            members.Add(new Member() { Name = "Test" });
            return members;
        }
    }
}
