﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MockInitiateServicesTests.MockInitiateService;

namespace MockInitiateServicesTests
{
    [TestClass]
    public class UnitTests
    {
        private static IdentityHubPortClient _identityHubPortClient;

        private TestContext testContextInstance;
        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// This property is automatically set by the testing framework
        /// TestContext.WriteLine will output to the test results console window
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            _identityHubPortClient = new IdentityHubPortClient();
        }
    

        [TestMethod]
        public void TestMethod1()
        {
            var members = _identityHubPortClient.MemberGetRequest();

            foreach (var member in members)
            {
               
                TestContext.WriteLine("Member name = {0}", member.Name);
            }
        }
    }
    

}
