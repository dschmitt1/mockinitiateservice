﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Imprivata.PatientSecure.Domain.Database.DTO;
using Imprivata.PatientSecure.Domain.Database.Persistence;
using MockInitiateServiceLib.DataAccess;

namespace MockInitiateServiceLib
{
    public class DAL
    {
        private static string connectionString =
       //   @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\dschmitt\Source\Repos\MockInitiateService\MockInitiateServiceLib\InitiateServiceDatabase.mdf;Integrated Security=True;Connect Timeout=30";
            @"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\dschmitt\Documents\InitiateServiceData.mdf;Integrated Security=True;MultipleActiveResultSets=True;Connect Timeout=30;Application Name=EntityFramework";

        public static Person GetPerson()
        {

            var lightRepository = new LightRepository(connectionString);

             const string sql = @"select * from Persons";
            var person = lightRepository.GetBySql<Person>(sql);

            return person.FirstOrDefault();
        }

        
          
    }
}
