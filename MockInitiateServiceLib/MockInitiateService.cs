﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MockInitiateServiceLib
{

    public class MockInitiateService : IIdentityHubPort
    {
        public List<Member> MemberGetRequest()
        {
            var person = DAL.GetPerson();

            var members = new List<Member>();


            members.Add(new Member(){Name = person.LastName});


            return members;

        }

        public List<Member> MemberSearchRequest()
        {
            var members = new List<Member>() { new Member() { Name = "Test" } };
            return members;
        }
    }
}
