﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MockInitiateServiceLib
{
    [ServiceContract]
    public interface IIdentityHubPort
    {
        [OperationContract]
        List<Member> MemberGetRequest();

        [OperationContract]
        List<Member> MemberSearchRequest();
    }


    [DataContract]
    public class Member
    {
        [DataMember]
        public string Name { get; set; }
    }
}
