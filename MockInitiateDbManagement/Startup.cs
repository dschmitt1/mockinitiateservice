﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MockInitiateDbManagement.Startup))]
namespace MockInitiateDbManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
