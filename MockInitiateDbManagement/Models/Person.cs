﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

using MockInitiateDbManagement;
using MockInitiateDbManagement.DataValidation;

namespace MockInitiateDbManagement
{
    [MetadataType(typeof(PersonMetaData))]
    public partial class Person
    {
      
    }
    
    public class PersonMetaData
    {
        [Required(ErrorMessage = "First Name is required")]
        [Display(Name = "First Name")]
        public string FistName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "DOB")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> DateOfBirth { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Display(Name = "Enterprise ID")]
        public System.Guid EnterpriseId { get; set; }
    }


}